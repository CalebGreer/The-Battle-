﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CustomPosition : MonoBehaviour, IPunObservable
{

    private float distance; 
    private float angle; 

    private PhotonView PV;
    private Rigidbody rb;

    private Vector3 dir;
    private Vector3 networkPosition;
    private Vector3 cachedPosition;

    private Vector3 position;

    private Quaternion rotation; 

    // Use this for initialization
    void Start() 
    {
        PV = GetComponent<PhotonView>();

        cachedPosition = transform.position;
        networkPosition = Vector3.zero;

        rotation = Quaternion.identity;

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        position = Vector3.MoveTowards(transform.position, networkPosition, distance * (1.0f / PhotonNetwork.SerializationRate));
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, angle * (1.0f / PhotonNetwork.SerializationRate));

        rb.MovePosition(position);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            //position
            stream.SendNext(rb.velocity);
            this.dir = transform.position - cachedPosition;
            cachedPosition = transform.position;

            stream.SendNext(transform.position);
            stream.SendNext(dir);

            //rotation
            stream.SendNext(rb.rotation);
        }
        else
        {
            //position 
            rb.velocity = (Vector3)stream.ReceiveNext();

            networkPosition = (Vector3)stream.ReceiveNext();
            dir = (Vector3)stream.ReceiveNext();

            float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
            networkPosition += dir * lag;

            distance = Vector3.Distance(transform.position, networkPosition);

            //rotation
            rotation = (Quaternion)stream.ReceiveNext();
            angle = Quaternion.Angle(transform.rotation, rotation);
        }
    }
}
