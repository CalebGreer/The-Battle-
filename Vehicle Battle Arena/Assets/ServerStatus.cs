﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;

public class ServerStatus : MonoBehaviour
{ 
    public int countOfPlayersOnMaster;
    public int countOfPlayersInRooms;
    public int countOfPlayers;
    public int countOfRooms;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        countOfPlayersOnMaster = PhotonNetwork.CountOfPlayersOnMaster;
        countOfPlayersInRooms = PhotonNetwork.CountOfPlayersInRooms;
        countOfPlayers = PhotonNetwork.CountOfPlayers;
        countOfRooms = PhotonNetwork.CountOfRooms;
    }
}
