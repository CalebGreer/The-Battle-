﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMgr : MonoBehaviour
{

    public static UIMgr instance;

    public GameObject deathCanvas;
    public GameObject paintCanvas;
    public GameObject GameOverCanvas;
    public Text winText;
    public Text looseText;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
            instance = this;
        } 
    } 
}
