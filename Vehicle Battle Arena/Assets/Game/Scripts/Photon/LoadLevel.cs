﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LoadLevel : MonoBehaviour
{
    public string sceneToLoad;

    public void loadLevel()
    {
        Room.instance.load(sceneToLoad);
        PhotonNetwork.CurrentRoom.IsOpen = false;
    }
}
