﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class CharacterSelection : MonoBehaviour
{ 
    public GameObject kickBtns;
    public GameObject startBtn;

    private PhotonView PV;

    public List<GameObject> cars = new List<GameObject>();
    public List<GameObject> kBtns = new List<GameObject>(); 

    private GameObject car;

    // Use this for initialization
    void Start()
    { 
        if(PhotonNetwork.IsMasterClient)
        { 
            kickBtns.SetActive(true);
            startBtn.SetActive(true);
        }
        else
        { 
            kickBtns.SetActive(false);
            startBtn.SetActive(false);
        }

        PV = GetComponent<PhotonView>();
        PV.RPC("RPC_Spawn", RpcTarget.AllBuffered, Room.instance.number - 1); 
    }

    [PunRPC]
    public void RPC_Spawn(int playerNumber)
    { 
        car = cars[playerNumber]; 
        car.SetActive(true); 
        kBtns[playerNumber].SetActive(true); 
    }

    public void LeaveGame()
    { 
        KickPlayer(Room.instance.number); 
        PhotonNetwork.LeaveRoom();
    }

    public void Bot()
    { 
        Room.instance.isBot = !Room.instance.isBot;
    }

    public void KickPlayer(int playerNumber)
    {
        PV.RPC("RPC_Despawn", RpcTarget.All, playerNumber - 1);
    }

    [PunRPC]
    public void RPC_Despawn(int playerNumber)
    { 
        cars[playerNumber].SetActive(false); 
        kBtns[playerNumber].SetActive(false); 
    } 
}
