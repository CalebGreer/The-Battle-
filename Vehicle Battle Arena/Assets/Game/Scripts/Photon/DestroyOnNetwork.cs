﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DestroyOnNetwork : MonoBehaviourPun
{
    public static DestroyOnNetwork instance;

    void Start()
    {
        if (instance == null)
        {
            instance = this; 
        }
        else
        {
            Destroy(instance);
            instance = this;
        }
    }

    public void Destroy(GameObject gObj)
    {
        PhotonNetwork.Destroy(gObj);
    }
}