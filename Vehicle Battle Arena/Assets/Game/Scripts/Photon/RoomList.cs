﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomList : MonoBehaviourPunCallbacks
{ 
    public GameObject errorPanel;
    public Text errorText;
    public GameObject roomListPanel;
    public GameObject contentArea;
    public GameObject button; 

	private List<RoomInfo> rooms = new List<RoomInfo>();

	void OnEnable()
	{
		base.OnEnable();
	}

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
		rooms = roomList;

        foreach (Transform child in contentArea.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (var roomInfo in rooms)
        {
            if (roomInfo.IsOpen)
            {
                ButtonInfo btn = Instantiate(button, contentArea.transform).GetComponent<ButtonInfo>();
                btn.roomName = roomInfo.Name;
            }
        }
    }

    public void ShowRoomList()
    {
        roomListPanel.SetActive(true);
    }

    public void CreateRoom(InputField field)
    {
        Lobby.instance.CreateAndJoin(field.text);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        errorPanel.SetActive(true);
        errorText.text = message;
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        errorPanel.SetActive(true);
        errorText.text = message;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorPanel.SetActive(true);
        errorText.text = message;
    }

	public override void OnCreatedRoom()
	{ 
		Debug.Log("here");
	}

	public override void OnDisconnected(DisconnectCause cause)
	{
        errorPanel.SetActive(true); 
		errorText.text = cause.ToString();
	}
}
