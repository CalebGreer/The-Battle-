﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInfo : Button
{ 
    public string roomName;
    public bool isRoomOpen;
    public int maxRoomSize;
    public int playersInRoom;

    private Text roomLabel; 

    public override void OnPointerClick(PointerEventData eventData)
    {
        Lobby.instance.JoinRoom(roomName);
    } 

    protected override void Start()
    { 
        roomLabel = transform.Find("Room Name").gameObject.GetComponent<Text>();
    }

    void Update()
    {
        roomLabel.text = roomName; 
    }
}
