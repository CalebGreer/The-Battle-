﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class NetworkCallbacks : MonoBehaviourPunCallbacks 
{ 
    byte kickCode = 0; 
        
    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.NetworkingClient.EventReceived += OnKick; 
    }
    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.NetworkingClient.EventReceived -= OnKick; 
    }

    void Start()
    {
        Connect();
    } 

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
        PhotonNetwork.JoinLobby();
    }

    private void Connect()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Connected to Lobby");
    }

    public override void OnLeftLobby()
    {
        Debug.Log("Left the Lobby");
    }

    public void OnKick(EventData photonEvent)
    {
        byte code = photonEvent.Code;
        if (code != kickCode)
        {
            return;
        }

        Debug.Log("Kicking");
        object[] data = (object[])photonEvent.CustomData;
        if (Room.instance.number == (int)data[0])
        {
            PhotonNetwork.LeaveRoom();
        }
    }
}
