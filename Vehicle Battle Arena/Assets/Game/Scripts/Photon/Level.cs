﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using Photon.Pun;
using System.IO;

public class Level : MonoBehaviourPunCallbacks, IInRoomCallbacks
{

    public static Level instance;

    public List<Transform> spawnPoints = new List<Transform>();

    public List<Player> players = new List<Player>(); 
    
    public Player customPlayer;

    public PlayerLogic player;

    private PhotonView PV;


    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this; 
        }
        else
        {
            Destroy(instance);
            instance = this;
        }

        PV = GetComponent<PhotonView>();

        if (PhotonNetwork.IsMasterClient)
        {
            LevelInit();
        }

        players.AddRange(PhotonNetwork.PlayerList);
        customPlayer = players.Find(x => x.ActorNumber == Room.instance.number);
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
    } 

    private void RPC_CreatePlayer()
    {
        PV.RPC("RPC_SpawnPlayers", RpcTarget.All);
    }

    [PunRPC]
    private void RPC_SpawnPlayers()
    {
        GameObject obj;

        if (Room.instance.isBot)
        {
            obj = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "AI Car")
                                                        , spawnPoints[Room.instance.number - 1].transform.position
                                                        , Quaternion.identity); 
        }
        else
        {
            obj = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonCar")
                                                        , spawnPoints[Room.instance.number - 1].transform.position
                                                        , Quaternion.identity);
        }

        player = obj.GetComponentInChildren<PlayerLogic>();
        player.playerNumber = Room.instance.number - 1;
    }

    public void DamagePlayer(PlayerLogic player, int dmg)
    {
        object[] data = { player.PV.ViewID, dmg };
        PV.RPC("RPC_TakeDamage", RpcTarget.All, data);
    }

    [PunRPC]
    private void RPC_TakeDamage(object[] data)
    {
        if((int)data[0] == player.PV.ViewID)
        { 
            player.TakeDamage((int)data[1]);
        }
    }

    private void LevelInit()
    {
        RPC_CreatePlayer();
    }

    public void SetDeathStatus(int number)
    {
        //PV.RPC("RPC_Died", RpcTarget.All, number);
        Player p = players.Find(x => x.ActorNumber == number);
        p.IsAlive = false;

        if (players.FindAll(x => x.IsAlive == true).Count < 2)
        {
            UIMgr.instance.GameOverCanvas.SetActive(true);
            if (customPlayer.IsAlive)
            {
                UIMgr.instance.winText.gameObject.SetActive(true);
            }
            else
            {
                UIMgr.instance.looseText.gameObject.SetActive(true);
            }
        } 
    }

    public void ReturnToMenu()
    {
        PhotonNetwork.LeaveRoom();
    }
}
