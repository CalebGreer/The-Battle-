﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

[RequireComponent(typeof(NetworkCallbacks))]
public class Lobby : MonoBehaviour
{
    public static Lobby instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance != this)
            { 
                Destroy(instance);
                instance = this;
            }
        }
    } 

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    } 

    public void CreateAndJoin(string roomName)
    {
        CreateRoom(roomName);
    }

    private void CreateRoom(string roomName)
    {
        RoomOptions roomOps = new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = (byte)MultiplayerSetting.instance.maxPlayers
        };

        PhotonNetwork.CreateRoom(roomName, roomOps);
    } 
}
