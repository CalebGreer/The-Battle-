﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMngr : MonoBehaviour
{ 
    public static CameraMngr instance;

    public Camera deathCam2D;
    public Camera deathCam;

    public List<GameObject> playerCams = new List<GameObject>();

    public GameObject mainCamera;

    private int index;

    // Use this for initialization
    void Start()
    { 
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
            instance = this;
        }

        deathCam.gameObject.SetActive(false);
        deathCam2D.gameObject.SetActive(false);
    } 

    public void SetDeathCam()
    {
        playerCams[index].SetActive(true);

        UIMgr.instance.deathCanvas.SetActive(true);
 //       deathCam.gameObject.SetActive(true);
 //       mainCamera.SetActive(false);
    }

    public void SetDeathCam2D()
    {
        deathCam.enabled = true; 
    } 

    public void NextPlayer()
    {
        //  playerCams[index].SetActive(false);
        ToggleCameras();

        index++;
        if(index > playerCams.Count - 1)
        {
            index = 0;
        }

        ToggleCameras();
     //   playerCams[index].SetActive(true);
    }

    public void PrevPlayer()
    {
        ToggleCameras();
   //     playerCams[index].SetActive(false);

        index--;
        if(index < 0)
        {
            index = playerCams.Count - 1;
        }
 //       playerCams[index].SetActive(false);
        ToggleCameras();
    }

    private void ToggleCameras()
    { 
        playerCams[index].SetActive(!playerCams[index].activeSelf); 
    } 
}
