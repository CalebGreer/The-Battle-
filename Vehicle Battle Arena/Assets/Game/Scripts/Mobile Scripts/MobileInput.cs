﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MobileInput : MonoBehaviour
{
#if UNITY_IOS
    private GameObject canvas;
    private int numTouches;
    private string objectTouched;

    private PlayerLogic playerLogic;
    private DrivingController driving;
    private GraphicRaycaster m_Raycaster;
    private PointerEventData m_PointerEventData;
    private EventSystem m_EventSystem;

    private void Start()
    {
        canvas = GameObject.Find("Canvas");
        m_Raycaster = canvas.GetComponent<GraphicRaycaster>();
        m_EventSystem = canvas.GetComponent<EventSystem>();
        playerLogic = GetComponent<PlayerLogic>();
        driving = GetComponent<DrivingController>();
    }

    private void Update()
    {
        numTouches = Input.touchCount;

        if (numTouches > 0)
        {
            for (int i = 0; i < numTouches; i++)
            {
                Touch touch = Input.GetTouch(i);

                if (touch.phase == TouchPhase.Began)
                {
                    m_PointerEventData = new PointerEventData(m_EventSystem);
                    m_PointerEventData.position = touch.position;
                    List<RaycastResult> results = new List<RaycastResult>();
                    m_Raycaster.Raycast(m_PointerEventData, results);

                    foreach (RaycastResult result in results)
                    {
                        Debug.Log("Tapped on: " + result.gameObject.name);
                        objectTouched = result.gameObject.name;

                        switch (objectTouched)
                        {
                            //Gas
                            case "A":
                                driving.mobileInput = 1.0f;
                                break;
                            //Break
                            case "B":
                                driving.mobileInput = -0.5f;
                                break;
                            //Use Item
                            case "X":
                                playerLogic.UseItem();
                                break;
                        }
                    }
                }

                else if (touch.phase == TouchPhase.Ended)
                {
                    m_PointerEventData = new PointerEventData(m_EventSystem);
                    m_PointerEventData.position = touch.position;
                    List<RaycastResult> results = new List<RaycastResult>();
                    m_Raycaster.Raycast(m_PointerEventData, results);

                    foreach (RaycastResult result in results)
                    {
                        Debug.Log("Tapped ended on: " + result.gameObject.name);
                        objectTouched = result.gameObject.name;

                        switch (objectTouched)
                        {
                            //Gas
                            case "A":
                                driving.mobileInput = 0.0f;
                                break;
                            //Break
                            case "B":
                                driving.mobileInput = 0.0f;
                                break;
                        }
                    }
                }
            }
        }
    }
#endif
}
