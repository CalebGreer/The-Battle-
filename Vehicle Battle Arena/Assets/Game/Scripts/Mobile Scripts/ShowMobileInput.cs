﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMobileInput : MonoBehaviour
{

    private void Start()
    {
#if MOBILE_INPUT
        this.gameObject.SetActive(true);
#else
        this.gameObject.SetActive(false);
#endif
    }
}