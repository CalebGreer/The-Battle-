﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderSteeringBehaviour : MonoBehaviour
{
    public float duration;

    private float elapsedTime = 0.0f;   // The max time of a walking session
    private float wait = 0.0f;          // Time since started walk
    private float waitTime = 0.0f;      // Wait this much time

    private float randomX;              // Randomly go this X Direction
    private float randomZ;              // Randomly go this Z Direction

    public bool move = true;                   // Determine whether moving or not

	void Start ()
    {
        randomX = Random.Range(-5, 5);
        randomZ = Random.Range(-5, 5);
	}
	
	void Update ()
    {
		if (elapsedTime < duration && move)
        {
            // Using randomZ for the Y position since the objected is rotated -90 degrees along the X-Axis
            transform.Translate(new Vector3(randomX, randomZ, 0) * Time.deltaTime);
            elapsedTime += Time.deltaTime;
        }
        else if (move)
        {
            move = false;
            wait = Random.Range(1, 3);
            waitTime = 0.0f;
        }

        if (waitTime < wait && !move)
        {
            waitTime += Time.deltaTime;
        }
        else if (!move)
        {
            move = true;
            elapsedTime = 0.0f;
            randomX = Random.Range(-5, 5);
            randomZ = Random.Range(-5, 5);
        }
	}
}
