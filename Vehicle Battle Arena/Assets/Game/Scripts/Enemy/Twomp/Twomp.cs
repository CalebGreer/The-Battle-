﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twomp : MonoBehaviour
{
    public Animator anim;

    public GameObject stopPoint;
    
    public float speed = 2.0f;
    public float riseSpeed = 2.0f; 
    public float timeBeforeRise = 3.0f;
    private float remainingTime;

    public bool activate = false;

    private Vector3 startPosition; 
    private Vector3 newUpVector;

    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        anim = GetComponentInParent<Animator>();
        rb = GetComponent<Rigidbody>();

        startPosition = transform.position;
        newUpVector = Vector3.up; 

        activate = false; 

        remainingTime = timeBeforeRise;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void CheckPlayer()
    {
        if (activate)
        {
            anim.SetBool("Activated", true);
        }
    }

    public void Attack()
    { 
        newUpVector = newUpVector.normalized * speed * Time.deltaTime;
        rb.MovePosition(transform.position - newUpVector); 
    }

    public void Rise()
    {
        remainingTime -= Time.deltaTime;
        if (remainingTime <= 0)
        {
            newUpVector = newUpVector.normalized * riseSpeed * Time.deltaTime;
            rb.MovePosition(transform.position + newUpVector);
            if (Vector3.Distance(transform.position, startPosition) < 1.5f)
            {
                transform.position = startPosition;
                anim.SetBool("Activated", false);
            }
        }
    }
}
