﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheelController : MonoBehaviour
{
    LogitechGSDK.LogiControllerPropertiesData properties;

    public float xAxis;
    public float GasInput;
    public float BrakeInput;

    private void Start()
    {
        Debug.Log(LogitechGSDK.LogiSteeringInitialize(false));
    }

    private void Update()
    {
        if (LogitechGSDK.LogiUpdate() && LogitechGSDK.LogiIsConnected(0))
        {
            LogitechGSDK.DIJOYSTATE2ENGINES rec;
            rec = LogitechGSDK.LogiGetStateUnity(0);

            // Steering
            xAxis = rec.lX / 32768f; // -1 0 1

            // Gas
            if (rec.lY > 2)
            {
                GasInput = 0;
            }
            else if (rec.lY < 2)
            {
                GasInput = rec.lY / -32768f;
            }

            // Brake
            if (rec.lRz > 0)
            {
                BrakeInput = 0;
            }
            else if (rec.lRz < 0)
            {
                BrakeInput = rec.lRz / -32768f;
            }

        }
        else
        {
            Debug.LogWarning("No Steering Wheel connected!");
        }
    }
}
