﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseEnemyStateBehaviour : StateMachineBehaviour
{
    public float chaseDistance = 40.0f;
    public float usePaintTimer = 3.0f;

    private float usePaintTimerReference;
    private GameObject target;
    private NavMeshAgent agent;
    private PlayerLogic player;
    private EnemyAILogic logic;

    override public void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log(fsm.gameObject.transform.parent.name + "'s State: ChaseEnemy.");
        player = fsm.gameObject.GetComponentInParent<PlayerLogic>();
        agent = fsm.gameObject.GetComponentInParent<NavMeshAgent>();
        logic = fsm.gameObject.GetComponent<EnemyAILogic>();
        logic.GetAllPlayers();
        target = logic.DetermineChaseTarget();
        usePaintTimerReference = usePaintTimer;
    }

    override public void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (player.currentItem != null)
        {
            if (player.currentItem.name == "Banana")
            {
                fsm.SetBool("hasBanana", true);
            }

            agent.destination = target.transform.position;
        }

        if (logic.GetDistanceFromTarget(target) <= chaseDistance)
        {
            switch (player.currentItem.name)
            {
                case "RedShell":
                    player.UseItem();
                    Debug.Log("Using Red Shell.");
                    fsm.SetBool("hasItem", false);
                    break;
                case "GreenShell":
                    if (logic.IsTargetInFront(target) == true)
                    {
                        player.UseItem();
                        Debug.Log("Using Green Shell.");
                        fsm.SetBool("hasItem", false);
                    }
                    break;
                case "Paint":
                    usePaintTimer -= Time.deltaTime;
                    if (usePaintTimer <= 0.0f)
                    {
                        player.UseItem();
                        usePaintTimer = usePaintTimerReference;
                        Debug.Log("Using Paint");
                        fsm.SetBool("hasItem", false);
                    }
                    break;
                default:
                    Debug.Log("Something went wrong.");
                    break;
            }
        }
    }
}