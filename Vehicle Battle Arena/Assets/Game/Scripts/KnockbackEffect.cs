﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockbackEffect : MonoBehaviour
{
    public float force = 2.0f;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<DrivingController>() != null)
        {
            Vector3 pushDirection = other.transform.position - transform.position;
            pushDirection = -pushDirection.normalized;
            GetComponent<Rigidbody>().AddForce(pushDirection * force * 100);
        }
    }
}
