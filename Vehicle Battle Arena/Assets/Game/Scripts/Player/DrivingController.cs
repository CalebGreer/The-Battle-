﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DrivingController : MonoBehaviour
{
#if UNITY_STANDALONE_WIN
    LogitechGSDK.LogiControllerPropertiesData properties;
#endif

#if UNITY_IOS
    public JoystickAsset joystick;
#endif

    private Rigidbody body;

    private float xAxis;
    private float GasInput;
    private float BrakeInput;
    private float deadZone = 0.1f;                          // any input below the deadZone threshold is ignored

    public float mobileInput;

    public float groundedDrag = 3f;               // RigidBody drag factor depending on whether the buggy is grounded or in the air.
    public float maxVelocity = 50;
    public float hoverForce = 1000;
    public float gravityForce = 1000f;
    public float hoverHeight = 0.45f;
    public GameObject[] hoverPoints;

    public float forwardAcceleration = 8000f;
    public float reverseAcceleration = 4000f;
    private float thrust = 0f;

    public float turnStrength = 1000f;
    private float turnValue = 0f;
    private Quaternion turnRotation;

    private float spinSpeed;

    public GameObject[] frontWheels;
    public GameObject[] rearWheels;

    int layerMask;

    PhotonView PV;

    void Start()
    {
#if UNITY_IOS
        joystick = GameObject.Find("Fixed Joystick").GetComponent<JoystickAsset>();
#endif

        body = GetComponent<Rigidbody>();
        body.centerOfMass = Vector3.down;
        body.useGravity = false;

        layerMask = 1 << LayerMask.NameToLayer("Vehicle");
        layerMask = ~layerMask;

        PV = GetComponent<PhotonView>();
        Debug.Log(LogitechGSDK.LogiSteeringInitialize(false));
    }

    void Update()
    {
        if (PV.IsMine)
        {
#if UNITY_STANDALONE_WIN
            if (LogitechGSDK.LogiUpdate() && LogitechGSDK.LogiIsConnected(0))
            {
                SteeringWheelInput();
            }
            else
            {
                ControllerInput();
            }
#endif

#if UNITY_IOS
            MobileJoystickInput();
#endif
        }
    }

    void FixedUpdate()
    {
        // Hover Force
        RaycastHit hit;
        bool grounded = false;
        for (int i = 0; i < hoverPoints.Length; i++)
        {
            var hoverPoint = hoverPoints[i];
            if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight, layerMask))
            {
                //body.AddForceAtPosition(Vector3.up * hoverForce * (1.0f - (hit.distance / hoverHeight)), hoverPoint.transform.position);
                grounded = true;
            }
            else
            {
                // Self levelling - returns the vehicle to horizontal when not grounded
                if (transform.position.y > hoverPoint.transform.position.y)
                {
                    body.AddForceAtPosition(hoverPoint.transform.up * gravityForce, hoverPoint.transform.position);
                }
                else
                {
                    body.AddForceAtPosition(hoverPoint.transform.up * -gravityForce, hoverPoint.transform.position);
                }
            }
        }

        if (grounded)
        {
            body.drag = groundedDrag;
        }
        else
        {
            body.drag = 0.1f;
            thrust /= 100f;
            turnValue /= 100f;
        }

        // Handle Turn forces
        float turn = turnValue * turnStrength * Time.deltaTime;

        float tireRotation = Mathf.Clamp(turnValue * 45.0f, -45.0f, 45.0f);

        spinSpeed += thrust * Time.deltaTime;

        // Wheel Steering feedback
        frontWheels[0].transform.localEulerAngles = new Vector3(spinSpeed, tireRotation, -180);
        frontWheels[1].transform.localEulerAngles = new Vector3(spinSpeed, tireRotation, 0);

        rearWheels[0].transform.localEulerAngles = new Vector3(spinSpeed, 0, -180);
        rearWheels[1].transform.localEulerAngles = new Vector3(spinSpeed, 0, 0);

        // Handle Forward and Reverse forces
        if (thrust > 0)
        {
            turnRotation = Quaternion.Euler(0f, turn, 0f);
            body.AddForce(transform.forward * thrust);
            body.MoveRotation(body.rotation * turnRotation);
        }
        else if (thrust < 0)
        {
            turnRotation = Quaternion.Euler(0f, -turn, 0f);
            body.AddForce(transform.forward * thrust);
            body.MoveRotation(body.rotation * turnRotation);
        }

        // Limit max velocity
        if (body.velocity.sqrMagnitude > (body.velocity.normalized * maxVelocity).sqrMagnitude)
        {
            body.velocity = body.velocity.normalized * maxVelocity;
        }
    }

#if UNITY_STANDALONE_WIN
    void ControllerInput()
    {
        // Main Thrust
        thrust = 0.0f;
        float acceleration = Input.GetAxis("Vertical");
        if (acceleration > deadZone)
        {
            thrust = acceleration * forwardAcceleration;
        }
        else if (acceleration < -deadZone)
        {
            thrust = acceleration * reverseAcceleration;
        }

        // Turning
        turnValue = 0.0f;
        float turnAxis = Input.GetAxis("Horizontal");
        if (Mathf.Abs(turnAxis) > deadZone)
        {
            turnValue = turnAxis;
        }
    }

    void SteeringWheelInput()
    {
        LogitechGSDK.DIJOYSTATE2ENGINES rec;
        rec = LogitechGSDK.LogiGetStateUnity(0);

        // Steering
        turnValue = 0.0f;
        xAxis = rec.lX / 32768f; // -1 0 1
        if (Mathf.Abs(xAxis) > deadZone)
            turnValue = xAxis;

        // Main Thrust
        thrust = 0.0f;

        // Gas
        if (rec.lY > 2)
        {
            GasInput = 0;
        }
        else if (rec.lY < 2)
        {
            GasInput = rec.lY / -32768f;
            thrust = GasInput * forwardAcceleration;
        }

        // Brake
        if (rec.lRz > 0)
        {
            BrakeInput = 0;
        }
        else if (rec.lRz < 0)
        {
            BrakeInput = rec.lRz / 32768f;
            thrust = BrakeInput * reverseAcceleration;
        }
    }
#endif

#if UNITY_IOS
    void MobileJoystickInput()
    {
        // Main Thrust
        thrust = 0.0f;
        float acceleration = mobileInput;
        if (acceleration > deadZone)
        {
            thrust = acceleration * forwardAcceleration;
        }
        else if (acceleration < -deadZone)
        {
            thrust = acceleration * reverseAcceleration;
        }

        // Turning
        turnValue = 0.0f;
        float turnAxis = joystick.Horizontal * 0.5f;
        if (Mathf.Abs(turnAxis) > deadZone)
        {
            turnValue = turnAxis;
        }
    }
#endif
}
