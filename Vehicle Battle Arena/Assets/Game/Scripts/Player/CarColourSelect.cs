﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CarColourSelect : MonoBehaviour {
    
    [Header("Colour Selection")]
    public int colourIndex;
    // These colors must be in the same order as the CarColor script
    public Color[] carColours; 

    public GameObject nextButton;
    public GameObject prevButton;

    private PhotonView PV; 
    private CharacterSelection cars;

    void Start()
    {
        cars = GetComponent<CharacterSelection>();
        if(!cars)
        {
            Debug.Log("Car is not set");
        }

        PV = GetComponent<PhotonView>();
        if (!PV)
        {
            Debug.LogWarning("There is no Phton View on " + gameObject.name);
        }

        if (PV)
        {
            colourIndex = Room.instance.number - 1;
            Room.instance.colorIndex = colourIndex;
            object[] data = { colourIndex, Room.instance.number - 1, PV.ViewID };
            PV.RPC("RPC_ChangeColor", RpcTarget.AllBuffered, data);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (colourIndex >= carColours.Length - 3)
        {
            nextButton.active = false;
        }
        if (colourIndex == 0)
        {
            prevButton.active = false;
        }
        if (colourIndex > 0)
        {
            if (!prevButton.active)
            {
                prevButton.active = true;
            }
        }
        if (colourIndex < carColours.Length - 3)
        {
            if (!nextButton.active)
            {
                nextButton.active = true;
            }
        }
    }

    public void NextColor()
    {
        if (colourIndex < carColours.Length - 3)
        {
            colourIndex++;
            Room.instance.colorIndex = colourIndex;//TODO: remove this line
            object[] data = { colourIndex, Room.instance.number - 1, PV.ViewID };
            PV.RPC("RPC_ChangeColor", RpcTarget.AllBuffered, data);
            //    carBody.SetTexture("_MainTex", carColours[colourIndex]);
        }
    }

    public void PrevColour()
    {
        if (colourIndex == carColours.Length - 1)
        {
            colourIndex -= 2;
            Room.instance.colorIndex = colourIndex;//TODO: remove this line
            object[] data = { colourIndex, Room.instance.number - 1, PV.ViewID };
            PV.RPC("RPC_ChangeColor", RpcTarget.AllBuffered, data);
        }
        else if (colourIndex > 0)
        {
            colourIndex--;
            Room.instance.colorIndex = colourIndex;//TODO: remove this line
            object[] data = { colourIndex, Room.instance.number - 1, PV.ViewID };
            PV.RPC("RPC_ChangeColor", RpcTarget.AllBuffered, data);
            //     carBody.SetTexture("_MainTex", carColours[colourIndex]);
        }
    }

    public void SetColour(int index)
    {
        colourIndex = index;
        Room.instance.colorIndex = colourIndex;
        object[] data = { colourIndex, Room.instance.number - 1, PV.ViewID };
        PV.RPC("RPC_ChangeColor", RpcTarget.AllBuffered, data);
    }

    [PunRPC]
    private void RPC_ChangeColor(object[] data)
    {
        //cars.cars[(int)data[1]].transform.Find("BODY1").GetComponent<MeshRenderer>().material.SetTexture("_MainTex", carColours[(int)data[0]]);

        Renderer[] parts = cars.cars[(int)data[1]].GetComponentsInChildren<Renderer>();

        foreach (Renderer part in parts)
        {
            foreach (Material mat in part.materials)
            {
                if (mat != null && mat.name.Contains("Steel"))
                {
                    mat.SetColor("_Color", carColours[(int)data[0]]);
                }
            }
        }
    }
}
