﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class VehicleController : MonoBehaviour
{
#if UNITY_STANDALONE_WIN
    LogitechGSDK.LogiControllerPropertiesData properties;
#endif

#if UNITY_IOS
    public JoystickAsset joystick;
#endif

    public PlayerLogic player;
    public PhotonView PV;

    public float Throttle { get; set; }
    public float Brake { get; set; }
    public float Boost { get; set; }
    public float Turn { get; set; }
    public bool IsDrift { get; set; }
    public bool IsJump { get; set; }

    private float deadZone = 0.1f;

    // Variables for Steering Wheel
    private float xAxis;
    private float GasInput;
    private float BrakeInput;


    private void Start()
    {
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
#if UNITY_IOS
        joystick = GameObject.Find("Fixed Joystick").GetComponent<JoystickAsset>();
#endif
            Debug.Log(LogitechGSDK.LogiSteeringInitialize(false));
        }
    }

    private void FixedUpdate()
    {
        if (PV.IsMine)
        {

#if UNITY_STANDALONE_WIN
            if (LogitechGSDK.LogiUpdate() && LogitechGSDK.LogiIsConnected(0))
            {
                SteeringWheelInput();
            }
            else
            {
                ControllerInput();
            }
#endif

#if UNITY_IOS
            MobileJoystickInput();
#endif
        }
    }

#if UNITY_STANDALONE_WIN
    void ControllerInput()
    {
        // floats
        //Boost = Input.GetButton("Boost") ? 1 : 0;
        Throttle = Input.GetAxis("Vertical");
        Turn = Input.GetAxis("Horizontal");

        // booleans
        IsDrift = Input.GetButton("Drift");
        //IsJump = Input.GetButton("Jump");

        // shooting
        if (player != null && Input.GetButton("Shoot"))
        {
            player.UseItem();
        }

        if (Input.GetButton("Flip"))
        {
            transform.rotation = new Quaternion(0, transform.rotation.y, 0, 0);
        }
    }

    void SteeringWheelInput()
    {
        LogitechGSDK.DIJOYSTATE2ENGINES rec;
        rec = LogitechGSDK.LogiGetStateUnity(0);

        // Steering
        Turn = 0.0f;
        xAxis = rec.lX / 32768f; // -1 0 1
        if (Mathf.Abs(xAxis) > deadZone)
            Turn = xAxis;

        // Main Thrust
        Throttle = 0.0f;

        // Gas
        if (rec.lY > 2)
        {
            GasInput = 0;
        }
        else if (rec.lY < 2)
        {
            GasInput = rec.lY / -32768f;
            Throttle = GasInput;
        }

        // Brake
        if (rec.lRz > 0)
        {
            BrakeInput = 0;
        }
        else if (rec.lRz < 0)
        {
            BrakeInput = rec.lRz / 32768f;
            Throttle = BrakeInput;
        }

        // Drift
        if (rec.rglSlider[0] > 0)
        {
            IsDrift = false;
        }
        else if (rec.rglSlider[0] < 0)
        {
            IsDrift = true;
        }

        // Jumping
        //if (rec.rgbButtons[0] == 128)
        //{
        //    IsJump = true;
        //}
        //else
        //{
        //    IsJump = false;
        //}

        // Boostin
        //if (rec.rgbButtons[5] == 128)
        //{
        //    Boost = 1;
        //}
        //else
        //{
        //    Boost = 0;
        //}
    }
#endif

#if UNITY_IOS
    void MobileJoystickInput()
    {
        // Main Thrust
        Throttle = 0.0f;
        float acceleration = joystick.Vertical;
        if (acceleration > deadZone)
        {
            Throttle = acceleration;
        }
        else if (acceleration < -deadZone)
        {
            Throttle = acceleration;
        }

        // Turning
        Turn = 0.0f;
        float turnAxis = joystick.Horizontal;
        if (Mathf.Abs(turnAxis) > deadZone)
        {
            Turn = turnAxis;
        }
    }
#endif
}

