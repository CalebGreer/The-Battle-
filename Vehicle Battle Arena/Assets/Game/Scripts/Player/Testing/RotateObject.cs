﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public float RotationSpeed = 8.0f;

    void Update ()
    {
        // Slowly rotate the object arond its X axis at 1 degree/second.
        transform.Rotate(0, Time.deltaTime * RotationSpeed, 0, Space.World);
    }
}
