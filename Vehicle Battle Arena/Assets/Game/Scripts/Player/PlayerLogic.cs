﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;

public class PlayerLogic : MonoBehaviourPunCallbacks, IPunObservable
{
    #region Public Fields

    public bool bot = false;

    public int currentHealth = 3;
    public GameObject currentItem;

    public int playerNumber;

    public List<GameObject> healthOrbs;

    #endregion

    #region Private Fields

    [SerializeField]
    private int maxHealth = 3;

    [Tooltip("The Player's UI GameObject Prefab")]
    [SerializeField]
    private GameObject PlayerUiPrefab;

    public PhotonView PV;
    private byte blindCode = 3;

    private CanvasList canvas;

    #endregion

    #region MonoBehaviour Callbacks
    // Use this for initialization
    void Start()
    {
        PV = GetComponent<PhotonView>();

        // Resets health to full on game load
        currentHealth = maxHealth;

        if (PlayerUiPrefab != null && !bot && PV.IsMine)
        {
            GameObject _uiGo = Instantiate(PlayerUiPrefab) as GameObject;
            // #Warning: This is not working
            _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
        }
        else
        {
            Debug.LogWarning("<Color=Red><b>Missing</b></Color> PlayerUiPrefab reference on player Prefab.", this);
        }
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.NetworkingClient.EventReceived += Blind;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.NetworkingClient.EventReceived -= Blind;
    }

    // Update is called once per frame
    void Update()
    {
        HeartUpdate(); 
    }

    #endregion

    #region Public Methods

    public void UseItem()
    {
        if (currentItem != null)
        {
            currentItem.GetComponent<IPickup<Transform>>().UsePickup(this.gameObject.transform);

            currentItem = null;
        }
        else
        {
            return;
        }
    }

    public void TakeDamage(int damageValue)
    {
        // Deduct the damage dealt from the character's health
        currentHealth -= damageValue;
        //RemoveHeart();

        // if the character is out of health, die!
        if (currentHealth <= 0)
        {
            Die();
        }
    }


    public void Die()
    {
        currentHealth = 0;
        CameraMngr.instance.SetDeathCam();
        PhotonNetwork.Destroy(transform.parent.gameObject); 
    }

    void OnDestroy()
    { 
        Level.instance.SetDeathStatus(playerNumber + 1); 
    }

    #endregion

    #region Private Methods

    private void HeartUpdate()
    {
        if (healthOrbs.Count > currentHealth)
        {
            RemoveHeart();
        }
    }

    public void RemoveHeart()
    {
        Destroy(healthOrbs[currentHealth]);
    }

    #endregion

    #region Custom Photon CallBacks

    public void Blind(EventData photonEvent)
    {
        byte code = photonEvent.Code;
        if (code != blindCode)
        {
            return;
        }

        canvas = GameObject.Find("PaintCanvas").gameObject.GetComponent<CanvasList>();
        if (!canvas)
        {
            Debug.LogWarning("Could not Find the obstruction Canvas");
        }

        canvas.paintPanel.SetActive(true);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(playerNumber);
            stream.SendNext(currentHealth);
        }
        else
        {
            playerNumber = (int)stream.ReceiveNext();
            currentHealth = (int)stream.ReceiveNext();
        }
    }

    #endregion
}