﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

    #region Private Fields

    [Tooltip("UI Slider to display Player's Health")]
    [SerializeField]
    private Slider playerHealthSlider;
    [Tooltip("UI Image to display Player's Item")]
    [SerializeField]
    private Image playerItem;

    private PlayerLogic target;
    private Color itemColor;

    #endregion

    #region Public Fields



    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        this.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);
    }

    // Use this for initialization
    void Start()
    {
        itemColor = playerItem.color;
    }

    // Update is called once per frame
    void Update()
    {
        // Reflect the Player Health
        if (playerHealthSlider != null)
        {
            playerHealthSlider.value = target.currentHealth;
        }

        if (target.currentItem != null)
        {
            playerItem.sprite = Resources.Load<Sprite>(target.currentItem.name);
            itemColor.a = 1f;
            playerItem.color = itemColor;
        }
        else
        {
            playerItem.sprite = null;
            itemColor.a = 0f;
            playerItem.color = itemColor;
        }

        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    #endregion

    #region Public Methods

    public void SetTarget(PlayerLogic _target)
    {
        if (_target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayerManager target for PlayerUI.SetTarget.", this);
            return;
        }
        // Cache references for efficiency
        target = _target;
    }

    #endregion
}
