﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitPlayer : MonoBehaviour
{ 
    public Transform target;
    public float orbitDistance = 10.0f;
    public float orbitDegreesPerSec = 180.0f;

    void Orbit()
    {
        if (target != null)
        {
            // Keep us at orbitDistance from target
            Vector3 tempPos = target.position + (transform.position - target.position).normalized * orbitDistance;
            transform.position = new Vector3(tempPos.x, target.position.y, tempPos.z);
            transform.rotation = new Quaternion(target.rotation.x, transform.rotation.y, target.rotation.z, target.rotation.w);
            transform.RotateAround(target.position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
        }
    }

    // Update is called once per frame
    void Update()
    {

        // Call from LateUpdate instead...
        // Orbit();

    }
    // Call from LateUpdate if you want to be sure your
    // target is done with it's move.
    void LateUpdate()
    {

        Orbit();

    }
}
