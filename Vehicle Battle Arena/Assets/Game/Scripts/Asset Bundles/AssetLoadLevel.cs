﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class AssetLoadLevel : MonoBehaviour
{
    public string bundleName;

	void Update ()
    {
		
	}

    public void LevelFromBundle()
    {
        string path = Path.Combine(Application.streamingAssetsPath, bundleName);
        var myLoadedAssetBundle = AssetBundle.LoadFromFile(path);
        if (myLoadedAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle");
            return;
        }
        
        if (myLoadedAssetBundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = myLoadedAssetBundle.GetAllScenePaths();
            string sceneName = Path.GetFileNameWithoutExtension(scenePaths[0]);
            SceneManager.LoadScene(sceneName);
        }
    }
}
