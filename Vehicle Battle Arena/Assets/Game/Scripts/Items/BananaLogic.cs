﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BananaLogic : MonoBehaviour, IPickup<Transform>
{
    [SerializeField]
    private float spawnDistance = 2.0f;

    public void UsePickup(Transform _transform)
    {
        Vector3 spawnPos = _transform.position - _transform.forward * spawnDistance;

        PhotonNetwork.Instantiate("Prefabs/Items/"+this.gameObject.name, spawnPos, this.transform.rotation);
    }
}
