﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickup<T>
{
    void UsePickup(T transform);
}
	

