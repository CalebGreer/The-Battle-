﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Photon.Pun;

[RequireComponent(typeof(Collider))]
public class DamageTrigger : MonoBehaviour, IPunObservable {

    [SerializeField]
    private int damage = 1;

    public GameObject otherObj;

    PhotonView PV;

    public bool collided;

    // Use this for initialization
    void Start()
    {
        collided = false;
        PV = GetComponent<PhotonView>();
    }

    public void OnTriggerEnter(Collider other)
    {
        // if the obj that entered us is the hero, cache the reference
        // otherObj is a gameobject reference
        if (other.gameObject.GetComponentInParent<PlayerLogic>() != null)
        {
            otherObj = other.gameObject;

            if (!collided)
            {
                Level.instance.DamagePlayer(otherObj.GetComponentInParent<PlayerLogic>(), damage);
            }
            collided = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponentInParent<PlayerLogic>() != null)
        {
            // inserted!
            otherObj = other.gameObject;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (collided && PV.IsMine)
        {
            IsDestructable gObj = GetComponent<IsDestructable>();
            if (gObj)
            {
                gObj.DestroyInstance();
            }

            collided = false;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(collided);
        }
        else
        {
            collided = (bool)stream.ReceiveNext();
        }
    }
}
