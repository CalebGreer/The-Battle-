﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Photon.Pun;

public class RedShellLogic : MonoBehaviour, IPickup<Transform>
{
    public float speed = 60f;
    [SerializeField]
    private float spawnDistance = 2.0f;
    [SerializeField]
    private GameObject target;

    private FieldOfView fov;
    private SeekSteeringBehaviour ssb;

    public void OnCollisionEnter(Collision collision)
    {
        // TODO: Figure out destroying when hitting walls
    }

    void Start()
    {
        fov = GetComponent<FieldOfView>();
        ssb = GetComponent<SeekSteeringBehaviour>();
    }

    void Update()
    {
        if (fov.visibleTargets.Count > 0)
        {
            target = fov.visibleTargets[0].gameObject;
        }

        if (target)
        {
            ssb.SetTarget(target);
        }
    }

    public void UsePickup(Transform _transform)
    {
        Vector3 spawnPos = _transform.position + _transform.forward * spawnDistance;
        spawnPos.y += 0.5f;
        GameObject projectile = PhotonNetwork.Instantiate("Prefabs/Items/"+this.gameObject.name
                                                         , spawnPos
                                                         , _transform.rotation) as GameObject;
        
        Rigidbody rb2 = projectile.GetComponent<Rigidbody>();
        rb2.velocity = _transform.forward * speed;
    }
}
