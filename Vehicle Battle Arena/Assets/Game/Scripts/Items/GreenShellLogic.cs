﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GreenShellLogic : MonoBehaviour, IPickup<Transform>
{
    public float speed = 60f;
    [SerializeField]
    private int hitsLeft = 5;
    [SerializeField]
    private float spawnDistance = 2.0f;

    // Update is called once per frame
    void Update()
    {
        if (hitsLeft <= 0)
        {
            DestroyOnNetwork.instance.Destroy(gameObject);
            //Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        hitsLeft--;
    }

    public void UsePickup(Transform _transform)
    {
        Vector3 spawnPos = _transform.position + _transform.forward * spawnDistance;
        spawnPos.y += 0.5f;
        GameObject projectile = PhotonNetwork.Instantiate("Prefabs/Items/"+this.gameObject.name
                                                         , spawnPos
                                                         , _transform.rotation) as GameObject;

        Rigidbody rb2 = projectile.GetComponent<Rigidbody>();
        rb2.velocity = _transform.forward * speed;
    }
}
