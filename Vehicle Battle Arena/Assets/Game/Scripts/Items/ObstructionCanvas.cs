﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObstructionCanvas : MonoBehaviour
{
    public Texture text;

    public GameObject panel;

    public float fadeDuration;
    public float timeTilFade;
    private float baseDuration;
    private float baseTilFadeDuration; 

    private Image img;

    private bool fadding;
    private bool fadded;
    // Use this for initialization
    void OnEnable()
    { 
		fadded = false;
        fadding = false;

        if (!img)
        {
            img = panel.GetComponent<Image>();
        } 

        baseDuration = fadeDuration;
        baseTilFadeDuration = timeTilFade;
    }

    // Update is called once per frame
    void Update()
    {
        timeTilFade -= Time.deltaTime; 
        if (timeTilFade <= 0.0f)
        {
            if (!fadding)
            {
                CrossFade();
            }
            FaddedUpdate();
        }
    }

    private void FaddedUpdate()
    {
        baseDuration -= Time.deltaTime;
        fadded = (baseDuration <= 0.0f) ? true : false;
        if (fadded)
        {
            this.gameObject.SetActive(false);
            timeTilFade = baseTilFadeDuration;
            fadding = false;
        }
    }

    private void CrossFade()
    {
        img.CrossFadeAlpha(0, fadeDuration, false);
        fadding = true;
    }
}
