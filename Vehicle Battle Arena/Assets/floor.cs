﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floor : MonoBehaviour {

    public Twomp thwomp;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    { 
        if(other.gameObject.GetComponentInParent<PlayerLogic>())
        {
            thwomp.activate = true;
        }

        if(other.gameObject.GetComponentInParent<Twomp>())
        {
            thwomp.anim.SetTrigger("GoUp");
        }
    }
}
