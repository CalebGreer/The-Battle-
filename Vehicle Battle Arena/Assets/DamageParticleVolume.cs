﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageParticleVolume : MonoBehaviour
{
    [SerializeField]
    private int damage = 1;

    //Makes sure we only deal 1 damage
   	private bool damaged;

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
		damaged = false;

        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

	void Update()
	{
		if(!part.IsAlive())
		{ 
			Destroy(transform.parent.gameObject);
		}
	}

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        PlayerLogic player = other.gameObject.GetComponentInParent<PlayerLogic>();

        int i = 0;

        while (i < numCollisionEvents)
        {
            if (player && !damaged)
            {
				damaged = true;
                Level.instance.DamagePlayer(player, damage);
            }
            i++;
        }
    }
}
