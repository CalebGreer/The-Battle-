﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheelConfig : MonoBehaviour {

    public float LeftStickHorizontal;
    public float LeftStickVertical;

    public float RightStickHorizontal;
    public float RightStickVertical;

    public float DPadHorizontal;
    public float DPadVertical;

    public float Triggers;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        LeftStickHorizontal = Input.GetAxis("Horizontal");
        LeftStickVertical = Input.GetAxis("Vertical");

        RightStickHorizontal = Input.GetAxis("Right Stick Horizontal");
        RightStickVertical = Input.GetAxis("Right Stick Vertical");

        DPadHorizontal = Input.GetAxis("D-Pad Horizontal");
        DPadVertical = Input.GetAxis("D-Pad Vertical");

        Triggers = Input.GetAxis("Triggers");

        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            Debug.Log("A Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            Debug.Log("B Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            Debug.Log("X Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            Debug.Log("Y Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button4))
        {
            Debug.Log("Left Bumper was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button5))
        {
            Debug.Log("Right Bumper was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button6))
        {
            Debug.Log("Back Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            Debug.Log("Start Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button8))
        {
            Debug.Log("Left Stick Button was Pressed");
        }
        if (Input.GetKeyDown(KeyCode.Joystick1Button9))
        {
            Debug.Log("Right Stick Button was Pressed");
        }

    }
}
